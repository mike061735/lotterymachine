﻿using System;

namespace LotteryMachineMain
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("請輸入開獎樂透球數：(最少1顆球，最多100顆球)");
            int input = int.Parse(Console.ReadLine());
            LotteryMachine lotteryMachine = new LotteryMachine(input);
            while (!lotteryMachine.CheckInput())
            {
                Console.WriteLine("輸入值超過範圍!!");
                Console.WriteLine("請重新輸入開獎樂透球數：(最少1顆球，最多100顆球)");
                input = int.Parse(Console.ReadLine());
                lotteryMachine = new LotteryMachine(input);
            }

            Console.WriteLine();
            Console.WriteLine("放入彩球....");
            Console.WriteLine();
            lotteryMachine.SetRandomBalls();
            Console.WriteLine("放入彩球完成");
            Console.WriteLine();

            Console.WriteLine("已設定開獎球數，接下來要?(請輸入數字)");
            Console.WriteLine("1.洗球");
            Console.WriteLine("2.開獎");
            Console.WriteLine("3.結束程式");
            Console.WriteLine();
            int inputSelect = int.Parse(Console.ReadLine());
            while (inputSelect != 3)
            {
                if (inputSelect == 1)
                {
                    Console.WriteLine();
                    Console.WriteLine("洗球中....");
                    Console.WriteLine();
                    lotteryMachine.Shuffle();
                    Console.WriteLine("洗球完成");
                    Console.WriteLine();
                }
                else if (inputSelect == 2)
                {
                    Console.WriteLine();
                    Console.WriteLine("開獎中...");
                    for (int i = 1; i <= input; i++)
                    { //依照輸入的球數數量，每次取出不重複的一顆球
                        Console.WriteLine();
                        Console.WriteLine("第{0}個開獎號碼：{1}", i, lotteryMachine.GetOneNumber());
                        Console.WriteLine();
                    }
                    Console.WriteLine("開獎完成");
                    break;

                }
                Console.WriteLine("已設定開獎球數，接下來要?(請輸入數字)");
                Console.WriteLine("1.洗球");
                Console.WriteLine("2.開獎");
                Console.WriteLine("3.結束程式");
                Console.WriteLine();
                inputSelect = int.Parse(Console.ReadLine());
            }
            Console.ReadLine();
        }
    }
}