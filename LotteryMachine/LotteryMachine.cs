﻿using System;
using System.Linq;

class LotteryMachine
{
    private int MaxAmountOfBall;
    private int[] Balls;
    private int min = 1;
    private int max = 100;
    private Random randNum = new Random();


    public LotteryMachine(int inputAmount)
    {
        this.MaxAmountOfBall = inputAmount;
        this.Balls = new int[inputAmount];
    }

    public int GetOneNumber()
    {
        //亂數index取球
        //將已取出的球的號碼回傳並在陣列中將該球改為0
        //判斷該球是否為0即可知道有沒有重複
        int pickBall = 0;
        while (true)
        {
            int randIdx = randNum.Next(min - 1, MaxAmountOfBall);
            if (Balls[randIdx] != 0)
            {
                pickBall = Balls[randIdx];
                Balls[randIdx] = 0;
                break;
            }
        }

        return pickBall;
    }

    public void SetRandomBalls()
    {
        //給定輸入的球數
        //先依照順序存入array
        //並且馬上洗球，達成亂數排列
        for (int i = 0; i < MaxAmountOfBall; i++)
        {
            Balls[i] = i + 1;
        }
        this.Shuffle();
    }

    public void Shuffle()
    {
        //隨機排列球的順序
        Balls = Balls.OrderBy(x => randNum.Next()).ToArray();
    }

    public Boolean CheckInput()
    {
        //檢查輸入的球數是否超過系統設定的球數
        if (MaxAmountOfBall <= max && MaxAmountOfBall >= min)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}